Average En-route delay per FAB in 2015.




TODOs:

- [x] automatic thresholding (quantization?). Use 5 classes instead of the 9 from Mike's bl.ock
- [x] proper legend, maybe using [d3.legend](http://d3-legend.susielu.com/#color-examples)
- [ ] Investigate adding FAB labels, i.e. in the relevant centroids, and assign automatically
      a high contrast (compared to area color [depending on delay value]) font color.
      See this [SO post](http://stackoverflow.com/a/407837/963575) or
      [this blog post](https://24ways.org/2010/calculating-color-contrast/).
      Also see [W3C material](https://www.w3.org/TR/AERT#color-contrast).
      The following simple function from the blog could suffice to decide between black or white label:
      
      ```javascript
      function getContrast50(hexcolor) {
        return (parseInt(hexcolor, 16) > 0xffffff/2) ? 'black':'white';
      }
      ```
- [ ] better tooltips, maybe using [d3.tip](https://github.com/caged/d3-tip)
      D3-tip has some benefits but:
      
      - not easy to have tooltip follow mouse, i.e. inside FAB area (see
        [issues #53](https://github.com/Caged/d3-tip/issues/53) for a possible solution)
      - no solution implemented to avoid cutoff at sides (see
        [issue #117](https://github.com/Caged/d3-tip/issues/117) for a possible fix not yet
        integrated in the official library)
      
      An alternative solution is the use of an helper, see
      [Chris Viau's gist](https://bl.ocks.org/biovisualize/2973775)
      or ([my fix](http://bl.ocks.org/espinielli/4d17fa15a7a5084e217992f985fba484) of
      [rveciana's one](http://bl.ocks.org/rveciana/5181105))

forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block:
<a href='http://bl.ocks.org/espinielli/7b66ad387b75d6077348'>European FABs</a>
